function getMyEle(id) {
    return document.getElementById(id);
}

// Bài tập 1
function timSoNhoNhat() {
    var sum = 0;
    for (i = 0; sum < 10000; i++) {
        sum += i;
        if (sum > 10000) {
            break;
        }
    }
    getMyEle("show-ket-qua-bt1").style.display = "block";
    getMyEle("show-ket-qua-bt1").innerHTML = `Số nguyên dương nhỏ nhất : ${i}`;
}

// Bài tập 2
function tinhTong() {
    var numberX = getMyEle("numberX").value * 1;
    var numberN = getMyEle("numberN").value * 1;
    var sum = 0;
    for (i = 1; i <= numberN; i++) {
        sum += Math.pow(numberX, i);
    }
    getMyEle("show-ket-qua-bt2").style.display = "block";
    getMyEle("show-ket-qua-bt2").innerHTML = `Tổng : ${sum}`
}

// Bài tập 3
function tinhGiaiThua() {
    var soNguyenDuong = getMyEle("soNguyenDuong").value * 1;
    var giaiThua = 1;
    for (i = 1; i <= soNguyenDuong; i++) {
        giaiThua *= i;
    }
    getMyEle("show-ket-qua-bt3").style.display = "block";
    getMyEle("show-ket-qua-bt3").innerHTML = `${soNguyenDuong}! = ${giaiThua}`
}

// Bài tập 4
function taoTheDiv() {
    var contentHTML = "";
    for (i = 1; i <= 10; i++) {
        if (i % 2 == 0) {
            var contentSoChan = `<div class="divChan"><span>Div chẵn</span></div>`;
            contentHTML += contentSoChan;
        } else {
            var contentSoLe = `<div class="divLe"><span>Div lẻ</span></div>`;
            contentHTML += contentSoLe;
        }
    }
    getMyEle("show-ket-qua-bt4").innerHTML = contentHTML;
}


